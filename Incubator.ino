/*
   (c) Fakufaku 2013
   (c) Kersnikova 2018
   This code is released under the CC-BY-SA 3.0 License.
*/

#include <DHT.h>
#include <LiquidCrystal_I2C.h>

// Heater constants
#define RELAY_ON  HIGH
#define RELAY_OFF LOW

// Temperature constants
#define TEMP_INCREMENT 1
#define TEMP_INITIAL 30
#define MIN_TEMP 15
#define MAX_TEMP 50

// Pin definitions
#define btn1Pin 2
#define btn2Pin 3
#define lightPin 5

// LCD over I2C
LiquidCrystal_I2C lcd(0x27, 16, 2);

// DHT22 temperature sensor
DHT dht(A0, 22); // Pin, Type(11 or 22)

float readTemperature() {
  float t = dht.readTemperature();
  // Check for NaN
  if (t != t) t = 0;
  return t;
}
float readHumidity() {
  float h = dht.readHumidity();
  // Check for NaN
  if (h != h) h = 0;
  return h;
}

// Control variables
float t_incub;              // Temperature in incubator
float t_set = TEMP_INITIAL; // Set point (Target temperature)
int disp_last_t_set = -1;
float disp_last_t_incub = -1;

void setup() {
  // Initialize LCD
  lcd.init();
  lcd.backlight();
  lcd.print("Initializing...");

  // Initialize light, and turn it off
  pinMode(lightPin, OUTPUT);
  digitalWrite(lightPin, HIGH);

  // Initialize buttons
  pinMode(btn1Pin, INPUT_PULLUP);
  pinMode(btn2Pin, INPUT_PULLUP);

  // Initialize temperature
  dht.begin();
  t_incub = readTemperature();

  lcd.print("Initialization complete.");
}

int relayValue = RELAY_OFF;
unsigned long relayTime = 0;
void temperatureControl() {
  t_incub = readTemperature();

  bool invalidValue = t_set <= 0 || t_set > MAX_TEMP || t_incub <= MIN_TEMP || t_incub > MAX_TEMP || t_incub != t_incub;
  int newRelayValue = (t_incub >= t_set || invalidValue) ? RELAY_OFF : RELAY_ON;
  if (relayValue != newRelayValue) {
    relayTime = millis();  
  }
  relayValue = newRelayValue;
  if (millis()-relayTime > 30000 && relayValue == RELAY_ON) {
    // Sleep for 4s every 30s
    digitalWrite(lightPin, RELAY_OFF);
    lcd.setCursor(0, 0);
    lcd.print("Relay pause...");
    disp_last_t_incub = -1; // Update display
    delay(4000);
    relayTime = millis();
  }
  digitalWrite(lightPin, relayValue);
}

void raiseTemperature() {
  if (t_set == 0) {
    t_set = (int)t_incub;
  } else if (t_set < MAX_TEMP) {
    t_set = ((int)(t_set + TEMP_INCREMENT) % MAX_TEMP);
  } else {
    t_set = MAX_TEMP;
  }
  if (t_set >= MAX_TEMP || t_set == 0) {
    t_set = MAX_TEMP;
  }
}
void lowerTemperature() {
  if (t_set == 0) {
    t_set = (int)t_incub;
  } else if (t_set > MIN_TEMP) {
    t_set = ((int)(t_set - TEMP_INCREMENT) % MAX_TEMP);
  } else {
    t_set = MIN_TEMP;
  }
}

char temp_str[5];
char humidity_str[5];
char set_str[5];
void displayTemperature() {
  if (disp_last_t_incub != t_incub || disp_last_t_set != t_set) {
    lcd.backlight();
    lcd.clear();
    lcd.print("  T:");
    if (t_incub == 0 || t_incub != t_incub) {
      temp_str[0] = '?';
      temp_str[1] = '\0';
    } else {
      dtostrf(t_incub, 0, 1, temp_str);
    }
    lcd.print(temp_str);
    lcd.print("C ");

    lcd.print("H:");
    int humidity = (int)readHumidity();
    if (humidity == 0) {
      humidity_str[0] = '?';
      humidity_str[1] = '\0';
    } else {
      itoa(humidity, humidity_str, 10);
    }
    lcd.print(humidity_str);
    lcd.print("%");

    lcd.setCursor(0, 1);
    lcd.print("Set:");
    itoa((int)t_set, set_str, 10);
    lcd.print(set_str);
    lcd.print("C");

    disp_last_t_set = t_set;
    disp_last_t_incub = t_incub;
  }
}

void handleButtons() {
  int b1 = digitalRead(btn1Pin);
  int b2 = digitalRead(btn2Pin);

  while (b1 == LOW || b2 == LOW) {
    b1 = digitalRead(btn1Pin);
    b2 = digitalRead(btn2Pin);
    if (b1 == LOW) raiseTemperature();
    if (b2 == LOW) lowerTemperature();
    displayTemperature();
    delay(400);
  }
}

void loop() {
  temperatureControl();
  handleButtons();
  displayTemperature();
  delay(50);
}
